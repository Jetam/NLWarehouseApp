﻿using System;

namespace WarehouseLibrary.Models
{
    public class LoginRequestModel
    {
        public string Username { get; set; }
        public byte[] Password { get; set; }
    }

    public class LoginResponseModel
    {
        public Guid Guid{ get; set; }
        public int Quantity { get; set; }
    }
}
