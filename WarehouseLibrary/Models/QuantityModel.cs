﻿using System;

namespace WarehouseLibrary.Models
{
    public class AddQuantityModel
    {
        public Guid Guid { get; set; }
        public int Quantity { get; set; }
    }
}
