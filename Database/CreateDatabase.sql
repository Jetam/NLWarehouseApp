USE [master]
GO
/****** Object:  Database [NLWarehouse]    Script Date: 10. 10. 2021 22:24:01 ******/
CREATE DATABASE [NLWarehouse]
 CONTAINMENT = NONE
 ON  PRIMARY 
( NAME = N'NLWarehouse', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL15.SQLEXPRESS01\MSSQL\DATA\NLWarehouse.mdf' , SIZE = 8192KB , MAXSIZE = UNLIMITED, FILEGROWTH = 65536KB )
 LOG ON 
( NAME = N'NLWarehouse_log', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL15.SQLEXPRESS01\MSSQL\DATA\NLWarehouse_log.ldf' , SIZE = 8192KB , MAXSIZE = 2048GB , FILEGROWTH = 65536KB )
 WITH CATALOG_COLLATION = DATABASE_DEFAULT
GO
ALTER DATABASE [NLWarehouse] SET COMPATIBILITY_LEVEL = 150
GO
IF (1 = FULLTEXTSERVICEPROPERTY('IsFullTextInstalled'))
begin
EXEC [NLWarehouse].[dbo].[sp_fulltext_database] @action = 'enable'
end
GO
ALTER DATABASE [NLWarehouse] SET ANSI_NULL_DEFAULT OFF 
GO
ALTER DATABASE [NLWarehouse] SET ANSI_NULLS OFF 
GO
ALTER DATABASE [NLWarehouse] SET ANSI_PADDING OFF 
GO
ALTER DATABASE [NLWarehouse] SET ANSI_WARNINGS OFF 
GO
ALTER DATABASE [NLWarehouse] SET ARITHABORT OFF 
GO
ALTER DATABASE [NLWarehouse] SET AUTO_CLOSE OFF 
GO
ALTER DATABASE [NLWarehouse] SET AUTO_SHRINK OFF 
GO
ALTER DATABASE [NLWarehouse] SET AUTO_UPDATE_STATISTICS ON 
GO
ALTER DATABASE [NLWarehouse] SET CURSOR_CLOSE_ON_COMMIT OFF 
GO
ALTER DATABASE [NLWarehouse] SET CURSOR_DEFAULT  GLOBAL 
GO
ALTER DATABASE [NLWarehouse] SET CONCAT_NULL_YIELDS_NULL OFF 
GO
ALTER DATABASE [NLWarehouse] SET NUMERIC_ROUNDABORT OFF 
GO
ALTER DATABASE [NLWarehouse] SET QUOTED_IDENTIFIER OFF 
GO
ALTER DATABASE [NLWarehouse] SET RECURSIVE_TRIGGERS OFF 
GO
ALTER DATABASE [NLWarehouse] SET  DISABLE_BROKER 
GO
ALTER DATABASE [NLWarehouse] SET AUTO_UPDATE_STATISTICS_ASYNC OFF 
GO
ALTER DATABASE [NLWarehouse] SET DATE_CORRELATION_OPTIMIZATION OFF 
GO
ALTER DATABASE [NLWarehouse] SET TRUSTWORTHY OFF 
GO
ALTER DATABASE [NLWarehouse] SET ALLOW_SNAPSHOT_ISOLATION OFF 
GO
ALTER DATABASE [NLWarehouse] SET PARAMETERIZATION SIMPLE 
GO
ALTER DATABASE [NLWarehouse] SET READ_COMMITTED_SNAPSHOT OFF 
GO
ALTER DATABASE [NLWarehouse] SET HONOR_BROKER_PRIORITY OFF 
GO
ALTER DATABASE [NLWarehouse] SET RECOVERY SIMPLE 
GO
ALTER DATABASE [NLWarehouse] SET  MULTI_USER 
GO
ALTER DATABASE [NLWarehouse] SET PAGE_VERIFY CHECKSUM  
GO
ALTER DATABASE [NLWarehouse] SET DB_CHAINING OFF 
GO
ALTER DATABASE [NLWarehouse] SET FILESTREAM( NON_TRANSACTED_ACCESS = OFF ) 
GO
ALTER DATABASE [NLWarehouse] SET TARGET_RECOVERY_TIME = 60 SECONDS 
GO
ALTER DATABASE [NLWarehouse] SET DELAYED_DURABILITY = DISABLED 
GO
ALTER DATABASE [NLWarehouse] SET QUERY_STORE = OFF
GO
USE [NLWarehouse]
GO
/****** Object:  User [harUser]    Script Date: 10. 10. 2021 22:24:01 ******/
CREATE USER [harUser] WITHOUT LOGIN WITH DEFAULT_SCHEMA=[dbo]
GO
/****** Object:  User [appUser]    Script Date: 10. 10. 2021 22:24:01 ******/
CREATE USER [appUser] FOR LOGIN [appUser] WITH DEFAULT_SCHEMA=[dbo]
GO
/****** Object:  UserDefinedFunction [dbo].[IsLoginActive]    Script Date: 10. 10. 2021 22:24:01 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


CREATE FUNCTION  [dbo].[IsLoginActive]
(
	@LoginId uniqueidentifier
)
RETURNS int
AS
BEGIN
	DECLARE @customerId int = NULL;

	SELECT @customerId = lo.CustomerId 
	FROM [dbo].[Login] lo
	WHERE lo.[Id] = @LoginId
	AND ((lo.ExpiryDate IS NULL) OR (lo.ExpiryDate > GETDATE()))

	return @customerId
END
GO
/****** Object:  Table [dbo].[AuditQuantity]    Script Date: 10. 10. 2021 22:24:01 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[AuditQuantity](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[CustomerId] [int] NOT NULL,
	[Quantity] [int] NOT NULL,
	[DateTime] [datetime2](7) NOT NULL,
 CONSTRAINT [PK_AuditQuantity] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Customer]    Script Date: 10. 10. 2021 22:24:01 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Customer](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Name] [nvarchar](50) NOT NULL,
	[Password] [binary](64) NOT NULL,
	[Quantity] [int] NOT NULL,
 CONSTRAINT [PK_Customer] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY],
 CONSTRAINT [UK_Customer] UNIQUE NONCLUSTERED 
(
	[Name] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[ErrorLog]    Script Date: 10. 10. 2021 22:24:01 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ErrorLog](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Error] [nvarchar](max) NOT NULL,
	[DateTime] [datetime2](7) NULL,
	[AdditionalData] [nvarchar](1000) NOT NULL,
 CONSTRAINT [PK_ErrorLOg] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Login]    Script Date: 10. 10. 2021 22:24:01 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Login](
	[Id] [uniqueidentifier] NOT NULL,
	[CustomerId] [int] NOT NULL,
	[CreatedOn] [datetime2](7) NOT NULL,
	[ExpiryDate] [datetime2](7) NULL,
 CONSTRAINT [PK_Login] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[Login]  WITH CHECK ADD  CONSTRAINT [FK_Login_Customer] FOREIGN KEY([CustomerId])
REFERENCES [dbo].[Customer] ([Id])
GO
ALTER TABLE [dbo].[Login] CHECK CONSTRAINT [FK_Login_Customer]
GO
/****** Object:  StoredProcedure [dbo].[Customer_AddQuantity]    Script Date: 10. 10. 2021 22:24:01 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[Customer_AddQuantity]
	@LoginId uniqueidentifier,
	@Quantity int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	
	declare @customerId INT;
	EXEC @customerId = [dbo].[IsLoginActive] @LoginId = @LoginId;

	IF (@customerId IS NOT NULL)
	BEGIN
		UPDATE Customer
		set Quantity = Quantity + @Quantity
		WHERE Id = @customerId

		INSERT INTO dbo.AuditQuantity (CustomerId, Quantity, [DateTime])
		VALUES (@customerId, @Quantity, GETDATE())

		SELECT Quantity
		FROM Customer
		WHERE Id = @customerId
	END
	ELSE
	BEGIN
		SELECT NULL AS Quantity;
	END
END
GO
/****** Object:  StoredProcedure [dbo].[Customer_GetByName]    Script Date: 10. 10. 2021 22:24:01 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[Customer_GetByName]
	@Name nvarchar(60)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

SELECT 
	c.Id as [CustomerId],
	c.[Name] as [Username],
	c.[Quantity],
	c.[Password]
FROM
	[dbo].[Customer] c
WHERE
	c.[Name] = @Name

END
GO
/****** Object:  StoredProcedure [dbo].[Customer_GetQuantity]    Script Date: 10. 10. 2021 22:24:01 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[Customer_GetQuantity]
	@Id int
AS
BEGIN
	SET NOCOUNT ON;
	SELECT Quantity FROM Customer WHERE [Id]=@Id
END
GO
/****** Object:  StoredProcedure [dbo].[Customer_Insert]    Script Date: 10. 10. 2021 22:24:01 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
create PROCEDURE [dbo].[Customer_Insert]
	@Password binary(64),
	@Name nvarchar(50)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	INSERT INTO [dbo].[Customer] ([Name], [Password])
	VALUES (@Name, @Password)

END
GO
/****** Object:  StoredProcedure [dbo].[ErrorLog_Insert]    Script Date: 10. 10. 2021 22:24:01 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[ErrorLog_Insert]
	@Error nvarchar(max),
	@AdditionalData nvarchar(1000)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	INSERT INTO [dbo].[ErrorLog] ([Error], [AdditionalData], [DateTime])
	VALUES (@Error, @AdditionalData, GETDATE())

END
GO
/****** Object:  StoredProcedure [dbo].[Login_CreateForCustomer]    Script Date: 10. 10. 2021 22:24:01 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
create PROCEDURE [dbo].[Login_CreateForCustomer]
	@CustomerId int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	DECLARE @expiryDate DateTIme2(7);
	DECLARE @guid uniqueidentifier;

	BEGIN
		SET @guid = NEWID();
		SET @expiryDate = DATEADD(WEEK, 1, GETUTCDATE());

		INSERT INTO [dbo].[Login] (Id, CustomerId, CreatedOn, ExpiryDate)
		VALUES (@guid, @CustomerId, GETUTCDATE(), @expiryDate)
	END

	SELECT @guid AS [Guid]

END
GO
USE [master]
GO
ALTER DATABASE [NLWarehouse] SET  READ_WRITE 
GO
