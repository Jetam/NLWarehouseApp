﻿using System;
using System.Threading.Tasks;
using WarehouseLibrary.Models;
using System.Configuration;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Windows;

namespace Client.Lib
{
    class RestRequestHelper
    {
        private static Guid Guid;
        Window refWindow; 
        public RestRequestHelper(Window window)
        {
            refWindow = window;
        }

        private static readonly HttpClient client = new HttpClient();

        public async Task Login(string username, byte[] password)
        {
            try
            {
                LoginRequestModel loginModel = new LoginRequestModel { Username = username, Password = password };
                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

                // HTTP POST
                var response = await client.PostAsJsonAsync(GetUrl("LoginUrl"), loginModel);
                var loginResponseModel = await response.Content.ReadAsAsync<Response<LoginResponseModel>>();
                if (response.IsSuccessStatusCode)
                {
                    if (!String.IsNullOrWhiteSpace(loginResponseModel.error))
                    {
                        ((LoginWindow)refWindow).lblError.Content = loginResponseModel.error;
                    }
                    else
                    {
                        RestRequestHelper.Guid = loginResponseModel.data.Guid;
                        MainWindow mainWindow = new MainWindow(loginResponseModel.data.Quantity);
                        mainWindow.Show();
                        refWindow.Close();
                    }
                }
                else
                {
                    await LogError(response.StatusCode.ToString());
                    ((LoginWindow)refWindow).lblError.Content = "Wrong status code";
                }
            }
            catch (Exception ex)
            {
                await LogError(ex.Message);
                ((LoginWindow)refWindow).lblError.Content = "Unknow error";
            }
        }
        private string GetUrl(string key)
        {
            return ConfigurationManager.AppSettings.Get("BaseUrl") + ConfigurationManager.AppSettings.Get(key);
        }

        public async Task AddQuantity(int quantity)
        {
            try
            {
                AddQuantityModel addQuantityModel = new AddQuantityModel { Guid = Guid, Quantity = quantity };
                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

                // HTTP POST
                var response = await client.PostAsJsonAsync(GetUrl("AddQuantityUrl"), addQuantityModel);
                var loginResponseModel = await response.Content.ReadAsAsync<Response<int>>();
                if (response.IsSuccessStatusCode)
                {
                    if (!String.IsNullOrWhiteSpace(loginResponseModel.error))
                    {
                        ((MainWindow)refWindow).lblError.Content = loginResponseModel.error;
                    }
                    else
                    {
                        ((MainWindow)refWindow).lblTotalQuantity.Content = Convert.ToString(loginResponseModel.data);
                    }
                }
                else
                {
                    await LogError(response.StatusCode.ToString());
                    ((MainWindow)refWindow).lblError.Content = "Wrong status code";
                }
            }
            catch(Exception ex)
            {
                await LogError(ex.Message);
                ((MainWindow)refWindow).lblError.Content = "Unknown error.";
            }
        }

        public async Task LogError(string error, string additionalData = "")
        {
            try
            {
                ErrorModel addQuantityModel = new ErrorModel { Error = error, AdditionalData = additionalData };
                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

                // HTTP POST
                var response = await client.PostAsJsonAsync(GetUrl("ErrorUrl"), addQuantityModel);
                //var loginResponseModel = await response.Content.ReadAsAsync<Object>();
            }
            catch
            {
            }
        }
    }
}
