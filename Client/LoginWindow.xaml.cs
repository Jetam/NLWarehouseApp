﻿using Client.Lib;
using System;
using System.Configuration;
using System.Windows;


namespace Client
{
    /// <summary>
    /// Interaction logic for LoginWindow.xaml
    /// </summary>
    public partial class LoginWindow : Window
    {
        public LoginWindow()
        {
            InitializeComponent();
        }

        private void btnLogin_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                lblError.Content = "";
                if (String.IsNullOrWhiteSpace(txtUsername.Text))
                {
                    lblError.Content = "Username is required. \n";
                }

                if (String.IsNullOrWhiteSpace(txtPassword.Password))
                {
                    lblError.Content += "Password is required.";
                }

                if (!String.IsNullOrWhiteSpace(lblError.Content as string))
                {
                    return;
                }

                RestRequestHelper requestHelper = new RestRequestHelper(this);
                byte[] passwordHash = SaltHash.Encrypt(txtPassword.Password, ConfigurationManager.AppSettings.Get("Salt"));
                var response = requestHelper.Login(txtUsername.Text, passwordHash);
            }
            catch (Exception ex)
            {
                lblError.Content = "Unknown error. Please try again.";
            }
        }
    }
}
