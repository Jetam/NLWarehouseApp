﻿using System;
using System.Windows;
using System.Windows.Input;
using System.Text.RegularExpressions;
using Client.Lib;

namespace Client
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow(int totalQuantity)
        {
            InitializeComponent();
            lblTotalQuantity.Content = totalQuantity;
        }

        private void TextBox_PreviewTextInput(object sender, TextCompositionEventArgs e)
        {
            Regex regex = new Regex("[^0-9]+");
            e.Handled = regex.IsMatch(e.Text);
        }

        private void btnAddQuantity_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                lblError.Content = "";
                if (String.IsNullOrWhiteSpace(txtQuantity.Text))
                {
                    lblError.Content = "No quantity";
                    return;
                }

                bool isInt = int.TryParse(txtQuantity.Text, out int quantity);
                if (!isInt)
                {
                    lblError.Content = "Quantity is not a valid number";
                    return;
                }

                RestRequestHelper requestHelper = new RestRequestHelper(this);
                var response = requestHelper.AddQuantity(quantity);
            }
            catch (Exception ex)
            {
                lblError.Content = "Unknown error. Please try again.";
            }

        }
    }
}