﻿
namespace WebService.Lib
{
    public class ErrorModel
    {
        public string Error { get; set; }
        public string AdditionalData { get; set; }
    }
}