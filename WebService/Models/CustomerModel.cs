﻿
namespace WebService.Models
{
    public class CustomerModel
    {
        public string Username { get; set; }
        public byte[] Password { get; set; }
        public int CustomerId { get; set; }
        public int Quantity { get; set; }
    }
}