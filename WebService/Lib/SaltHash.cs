﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Text;
using System.Security.Cryptography;
using System.Security.Policy;
using System.Runtime.Remoting.Messaging;

/// <summary>CHashProtector is a password protection one way encryption algorithm</summary>
/// <written>May 16, 2004</written>
/// <Updated>Aug 07, 2004</Updated>

public class SaltHash
{
	public enum HashEnum : int
	{
		// Supported algorithms
		SHA1,
		SHA256,
		SHA384,
		SHA512,
		MD5
	}

	public static byte[] Encrypt(string plainText, string salt = "", HashEnum hashEnum = HashEnum.SHA512)
	{
		HashAlgorithm cryptoService;
		salt = salt == String.Empty ? "default" : salt;
		switch (hashEnum)
		{
			case HashEnum.MD5:
				cryptoService = new MD5CryptoServiceProvider();
				break;
			case HashEnum.SHA1:
				cryptoService = new SHA1Managed();
				break;
			case HashEnum.SHA256:
				cryptoService = new SHA256Managed();
				break;
			case HashEnum.SHA384:
				cryptoService = new SHA384Managed();
				break;
			default:
				cryptoService = new SHA512Managed();
				break;
		}

		byte[] cryptoByte = cryptoService.ComputeHash(Encoding.GetEncoding(1250).GetBytes(plainText + salt));
		return cryptoByte;
	}

	public static bool Validate(string plainText, byte[] cryptoByte, string salt = "", HashEnum hashEnum = HashEnum.SHA512)
	{
		byte[] hash = Encrypt(plainText, salt, hashEnum);

		return Convert.ToBase64String(cryptoByte, 0, cryptoByte.Length) == Convert.ToBase64String(hash, 0, hash.Length);
	}
}
