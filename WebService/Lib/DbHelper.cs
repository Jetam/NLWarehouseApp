﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;

namespace WebService.Lib
{
    public class DbHelper
    {
        public static string ConnectionString = ConfigurationManager.ConnectionStrings["dbConnection"].ConnectionString;

        private static void SetDbValue(object value, SqlDbType? sType)
        {
            if (value == null)
            {
                value = DBNull.Value;
                return;
            }
            switch (sType)
            {
                case SqlDbType.Int:
                    value = String.IsNullOrEmpty(value.ToString()) ? (object)DBNull.Value : Convert.ToInt32(value);
                    break;

                case SqlDbType.Decimal:
                    value = String.IsNullOrEmpty(value.ToString()) ? (object)DBNull.Value : Convert.ToDecimal(value);
                    break;

                case SqlDbType.Float:
                    value = String.IsNullOrEmpty(value.ToString()) ? (object)DBNull.Value : Convert.ToDouble(value);
                    break;

                case SqlDbType.DateTime:
                    value = String.IsNullOrEmpty(value.ToString()) ? (object)DBNull.Value : Convert.ToDateTime(value);
                    break;
            }
        }
        public static void ExecuteNonQuery(string procName, DbParameters param)
        {
            SqlConnection conn = new SqlConnection(ConnectionString);
            SqlCommand cmd = new SqlCommand(procName, conn);
            cmd.CommandType = CommandType.StoredProcedure;

            if (param != null)
            {
                foreach (DbParameter par in param.GetParameters())
                {
                    string parameter = par.Name;
                    if (string.Compare(par.Name.Substring(0, 1), "@") != 0)
                    {
                        parameter = par.Name.Insert(0, "@");
                    }
                    SqlParameter sqlParam = new SqlParameter(parameter, par.Value);
                    sqlParam.SqlDbType = par.SqlType;
                    cmd.Parameters.Add(sqlParam);
                }
            }

            conn.Open();
            SqlTransaction trans = conn.BeginTransaction();
            cmd.Transaction = trans;

            try
            {
                cmd.ExecuteNonQuery();
                trans.Commit();
            }
            catch
            {
                trans.Rollback();
                throw;
            }
            finally
            {
                conn.Close();
            }
        }

        public static DataSet ExecuteDS(string procName, DbParameters param)
        {
            SqlConnection conn = new SqlConnection(ConnectionString);
            SqlCommand cmd = new SqlCommand(procName, conn);
            cmd.CommandType = CommandType.StoredProcedure;
            DataSet ds = new DataSet();

            if (param != null)
            {
                foreach (DbParameter par in param.GetParameters())
                {
                    string parameter = par.Name;
                    if (string.Compare(par.Name.Substring(0, 1), "@") != 0)
                    {
                        parameter = par.Name.Insert(0, "@");
                    }
                    SqlParameter sqlParam = new SqlParameter(parameter, par.Value);
                    sqlParam.SqlDbType = par.SqlType;
                    cmd.Parameters.Add(sqlParam);
                }
            }

            cmd.Connection.Open();
            cmd.Transaction = cmd.Connection.BeginTransaction();

            SqlDataAdapter da = new SqlDataAdapter(cmd);
            try
            {
                da.Fill(ds);
                cmd.Transaction.Commit();
            }
            catch (Exception ex)
            {
                cmd.Transaction.Rollback();
                throw;
            }
            finally
            {
                cmd.Connection.Close();
            }
            return ds;
        }

        public static DataTable Execute(string procName)
        {
            return ExecuteDt(procName, null);
        }
        public static DataTable ExecuteDt(string procName, DbParameters param)
        {
            SqlConnection conn = new SqlConnection(ConnectionString);
            SqlCommand cmd = new SqlCommand(procName, conn);
            cmd.CommandType = CommandType.StoredProcedure;
            DataTable dt = new DataTable();

            if (param != null)
            {
                foreach (DbParameter par in param.GetParameters())
                {
                    string parameter = par.Name;
                    if (string.Compare(par.Name.Substring(0, 1), "@") != 0)
                    {
                        parameter = par.Name.Insert(0, "@");
                    }
                    SqlParameter sqlParam = new SqlParameter(parameter, par.Value);
                    sqlParam.SqlDbType = par.SqlType;
                    cmd.Parameters.Add(sqlParam);
                }
            }

            cmd.Connection.Open();
            cmd.Transaction = cmd.Connection.BeginTransaction();

            SqlDataAdapter da = new SqlDataAdapter(cmd);
            try
            {
                da.Fill(dt);
                cmd.Transaction.Commit();
            }
            catch (Exception ex)
            {
                cmd.Transaction.Rollback();
                throw;
            }
            finally
            {
                cmd.Connection.Close();
            }
            return dt;
        }

        public static DataTable DbExecute(string procName, string paramName, object paramValue, SqlDbType? sType)
        {
            if (sType != null)
            {
                SetDbValue(paramValue, sType);
            }
            var conn = new SqlConnection(ConnectionString);
            var cmd = new SqlCommand(procName, conn);
            cmd.CommandType = CommandType.StoredProcedure;
            var dt = new DataTable();

            if ((paramName != null && paramName != string.Empty) && (paramValue != null))
            {
                cmd.Parameters.Add(new SqlParameter(paramName, paramValue));
            }

            conn.Open();
            var trans = conn.BeginTransaction();
            cmd.Transaction = trans;

            var da = new SqlDataAdapter(cmd);
            try
            {
                da.Fill(dt);
                trans.Commit();
            }
            catch (Exception)
            {
                trans.Rollback();
                throw;
            }
            finally
            {
                conn.Close();
            }
            return dt;
        }

        [Obsolete("Method is deprecated, please use DbParameters")]
        public static DataTable ExecuteCt(string commandText, Dictionary<string, object> param)
        {
            var conn = new SqlConnection(ConnectionString);
            var cmd = new SqlCommand(commandText, conn);
            //_cmd.CommandType = CommandType.Text;
            var dt = new DataTable();

            if (param != null)
            {
                foreach (KeyValuePair<string, object> kvp in param)
                {
                    string paramName = kvp.Key;
                    object value = kvp.Value;
                    if (string.Compare(paramName.Substring(0, 1), "@") != 0)
                    {
                        paramName = paramName.Insert(0, "@");
                    }
                    cmd.Parameters.Add(new SqlParameter(paramName, value));
                }
            }

            conn.Open();
            SqlTransaction trans = conn.BeginTransaction();
            cmd.Transaction = trans;

            var da = new SqlDataAdapter(cmd);
            try
            {
                da.Fill(dt);
                trans.Commit();
            }
            catch
            {
                trans.Rollback();
                throw;
            }
            finally
            {
                conn.Close();
            }
            return dt;
        }

    }

    public class DbParameters
    {
        private List<DbParameter> _list;

        public DbParameters()
        {
            _list = new List<DbParameter>();
        }

        public DbParameters(string name, object value, SqlDbType tip)
        {
            _list = new List<DbParameter>();
            DbParameter tmp = new DbParameter(name, value, tip);
            _list.Add(tmp);
        }

        public void Add(string name, object value, SqlDbType tip)
        {
            DbParameter tmp = new DbParameter(name, value, tip);
            _list.Add(tmp);
        }

        public void RemoveParameter(string name)
        {
            foreach (DbParameter param in _list)
            {
                if (param.Name == name)
                {
                    _list.Remove(param);
                    break;
                }
            }
        }

        protected DbParameter GetParameter(int i)
        {
            return _list[i];

        }

        public DbParameter GetParameter(string name)
        {
            foreach (DbParameter param in _list)
            {
                if (param.Name == name)
                    return param;
            }

            return null;
        }

        public List<DbParameter> GetParameters()
        {
            return _list;
        }

        public void Clear()
        {
            _list.Clear();
        }

        public int Length()
        {
            return _list.Count;
        }
    }

    public class DbParameter
    {
        public string Name { get; set; }
        public SqlDbType SqlType { get; set; }
        public object Value { get; set; }

        public DbParameter(string name, object value, SqlDbType sType)
        {
            Name = name;
            SqlType = sType;
            Value = value;
            SetDbValue();
        }

        public void SetDbValue()
        {
            if (Value == null)
            {
                Value = DBNull.Value;
                return;
            }
            switch (SqlType)
            {
                case SqlDbType.Int:
                    if (String.IsNullOrEmpty(Value.ToString()))
                        Value = DBNull.Value;
                    else
                        Value = Convert.ToInt32(Value);
                    break;

                case SqlDbType.Decimal:
                    if (String.IsNullOrEmpty(Value.ToString()))
                        Value = DBNull.Value;
                    else
                        Value = Convert.ToDecimal(Value);
                    break;

                case SqlDbType.Float:
                    if (String.IsNullOrEmpty(Value.ToString()))
                        Value = DBNull.Value;
                    else
                        Value = Convert.ToDouble(Value);
                    break;

                case SqlDbType.DateTime:
                    if (String.IsNullOrEmpty(Value.ToString()))
                        Value = DBNull.Value;
                    else
                        Value = Convert.ToDateTime(Value);
                    break;
            }
        }
    }
}