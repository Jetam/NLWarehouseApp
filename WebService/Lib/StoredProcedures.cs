﻿
namespace WebService.Lib
{
    public static class StoredProcedures
    {
        public const string CUSTOMER_ADDQUANTITY = "[dbo].[Customer_AddQuantity]";
        public const string LOGIN_CREATEFORCUSTOMER = "[dbo].[Login_CreateForCustomer]";
        public const string CUSTOMER_GETBYNAME = "[dbo].[Customer_GetByName]";
        public const string ERRORLOG_INSERT = "[dbo].[ErrorLog_Insert]";
        //public const string HARMTYPE_GET = "[dbo].[Customer_GetQuantity]";
    }
}