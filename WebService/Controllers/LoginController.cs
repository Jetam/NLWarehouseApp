﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Http;
using Newtonsoft.Json;
using System.Data;
using WebService.Lib;
using WebService.Models;
using WarehouseLibrary.Models;

namespace Server.Controllers
{
    public class LoginController : ApiController
    {
        [HttpPost]
        [Route("api/login/login")]
        public Response<LoginResponseModel> login([FromBody] LoginRequestModel loginModel)
        {
            //TEST
            //DbParameters parameters = new DbParameters();
            //parameters.Add("@Name", loginModel.Username, SqlDbType.VarChar);
            //parameters.Add("@Password", loginModel.Password, SqlDbType.Binary);

            //DataTable dtSong = DbHelper.ExecuteDt("[dbo].[Customer_Insert]", parameters);

            var response = new Response<LoginResponseModel>();
            response.data = new LoginResponseModel();

            if (string.IsNullOrWhiteSpace(loginModel.Username) || loginModel.Password == null || loginModel.Password.Length == 0)
            {
                response.error = "No username or password";
                return response;
            }

            DataTable dtCustomer = DbHelper.DbExecute(StoredProcedures.CUSTOMER_GETBYNAME, "Name", loginModel.Username, SqlDbType.NVarChar);
            string jsonString = JsonConvert.SerializeObject(dtCustomer);
            List<CustomerModel> customerModel = JsonConvert.DeserializeObject(jsonString, typeof(List<CustomerModel>)) as List<CustomerModel>;

            if (customerModel.Count == 0)
            {
                response.error = "Wrong username or password";
                return response;
            }
            bool passwordValid = Convert.ToBase64String(loginModel.Password, 0, loginModel.Password.Length) == Convert.ToBase64String(customerModel.FirstOrDefault().Password, 0, customerModel.FirstOrDefault().Password.Length);

            if (!passwordValid)
            {
                response.error = "Wrong username or password";
                return response;
            }
            else
            {
                try
                {
                    DataTable dtLogin = DbHelper.DbExecute(StoredProcedures.LOGIN_CREATEFORCUSTOMER, "CustomerId", customerModel.FirstOrDefault().CustomerId, SqlDbType.Int);
                    jsonString = JsonConvert.SerializeObject(dtLogin);
                    List<LoginDataModel> loginResponse = JsonConvert.DeserializeObject(jsonString, typeof(List<LoginDataModel>)) as List<LoginDataModel>;
                    response.data.Guid = loginResponse.FirstOrDefault().Guid;
                    response.data.Quantity = customerModel.FirstOrDefault().Quantity;
                    response.error = null;
                }
                catch (Exception ex)
                {
                    DbParameters parameters = new DbParameters();
                    parameters.Add("@Error", ex.Message, SqlDbType.NVarChar);
                    parameters.Add("@AdditionalData", "LoginController", SqlDbType.NVarChar);
                    DbHelper.ExecuteDt(StoredProcedures.ERRORLOG_INSERT, parameters);

                    response.error = "Unknown error, please try again";
                    return response;
                }
            }
            return response;
        }

        [HttpGet]
        ///https://localhost:44353/api/login/test
        public string test()
        {
            return "TEST";
        }
    }
}