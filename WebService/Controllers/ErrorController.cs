﻿using System;
using System.Data;
using System.Web.Http;
using WebService.Lib;

namespace WebService.Controllers
{
    public class ErrorController : ApiController
    {
        [HttpPost]
        [Route("api/error/insert")]
        public void insert([FromBody] ErrorModel errorModel)
        {
            try
            {
                DbParameters parameters = new DbParameters();
                parameters.Add("@Error", errorModel.Error, SqlDbType.NVarChar);
                parameters.Add("@AdditionalData", "ErrorController" + errorModel.AdditionalData, SqlDbType.NVarChar);
                DbHelper.ExecuteDt(StoredProcedures.ERRORLOG_INSERT, parameters);
            }
            catch (Exception ex)
            {
                DbParameters parameters = new DbParameters();
                parameters.Add("@Error", ex.Message, SqlDbType.NVarChar);
                parameters.Add("@AdditionalData", "ErrorController", SqlDbType.NVarChar);
                DbHelper.ExecuteDt(StoredProcedures.ERRORLOG_INSERT, parameters);
            }
        }
    }
}