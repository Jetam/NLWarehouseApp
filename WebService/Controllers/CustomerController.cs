﻿using System;
using System.Web.Http;
using System.Data;
using WarehouseLibrary.Models;
using WebService.Lib;

namespace Server.Controllers
{
    public class CustomerController : ApiController
    {
        [HttpPost]
        [Route("api/customer/addQuantity")]
        public Response<int> addQuantity([FromBody] AddQuantityModel quantityModel)
        {

            var response = new Response<int>();
            response.data = 0;
            if (quantityModel.Guid == null)
            {
                response.error = "Not authorized. Please log in again.";
                return response;
            }
            try
            {
                DbParameters parameters = new DbParameters();
                parameters.Add("@LoginId", quantityModel.Guid, SqlDbType.UniqueIdentifier);
                parameters.Add("@Quantity", quantityModel.Quantity, SqlDbType.Int);
                DataTable dtQuantity = DbHelper.ExecuteDt(StoredProcedures.CUSTOMER_ADDQUANTITY, parameters);
                response.data = (int)dtQuantity.Rows[0][0];
                response.error = null;
            }
            catch (Exception ex)
            {
                DbParameters parameters = new DbParameters();
                parameters.Add("@Error", ex.Message, SqlDbType.NVarChar);
                parameters.Add("@AdditionalData", "CustomerController", SqlDbType.NVarChar);
                DbHelper.ExecuteDt(StoredProcedures.ERRORLOG_INSERT, parameters);

                response.error = "Unknown error. Our developers has been informed. Please try again later.";
                return response;
            }
            return response;
        }
    }
}
